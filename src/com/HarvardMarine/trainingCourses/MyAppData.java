package com.HarvardMarine.trainingCourses;

import android.app.Application;
import com.testflightapp.lib.TestFlight;

public class MyAppData extends Application{
	@Override
    public void onCreate() {
        super.onCreate();
        //Initialize TestFlight with your app token.
        TestFlight.takeOff(this, "b40b574d-be52-418b-839d-fd4ac0eb34ba");
        // ...  
    }
}
