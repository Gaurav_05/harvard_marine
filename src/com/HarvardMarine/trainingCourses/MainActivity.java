package com.HarvardMarine.trainingCourses;


import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends Activity {

	ProgressDialog progressDialog = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		progressDialog = new ProgressDialog(MainActivity.this);
		progressDialog.setMessage("Loading ...");
		progressDialog.setCancelable(false);
		progressDialog.show();
		
		WebView myWebView = (WebView) findViewById(R.id.webview);
		myWebView.setInitialScale(1);
		myWebView.getSettings().setJavaScriptEnabled(true);
		myWebView.getSettings().setLoadWithOverviewMode(true);
		myWebView.getSettings().setUseWideViewPort(true);
		myWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		myWebView.setScrollbarFadingEnabled(false);
		myWebView.setWebViewClient(new WebViewClient());
		
		
		myWebView.setWebViewClient(new WebViewClient() {
			public void onPageFinished(WebView view, String url) {
		        // your code here
				progressDialog.hide();
		    }
		});
		
		myWebView.loadUrl("http://harvardmarineus.org/classes/");
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}*/
	
	@Override
	public void onStop(){
		super.onStop();
		if(progressDialog != null){
			progressDialog.dismiss();
		}
		
	}

}
